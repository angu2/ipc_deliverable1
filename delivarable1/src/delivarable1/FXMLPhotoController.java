/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivarable1;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jakub Mojto
 */
public class FXMLPhotoController {
    
    @FXML private ImageView imageView;
        
    public void initData (Image i ) {
      imageView.setImage(i);  
    }
}
