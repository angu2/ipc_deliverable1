/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivarable1;

import accesoaBD.AccesoaBD;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.Alumno;
import modelo.Curso;
import modelo.Dias;
import modelo.Matricula;

/**
 * FXML Controller class
 *
 * @author Ania
 */
public class FXMLCoursesController implements Initializable {

    ObservableList<Curso> cursosObservable;
    ObservableList<Matricula> matriculaObservable;
    ObservableList<Alumno> AlumnosEnCursoObservable;
    AccesoaBD acceso = new AccesoaBD();
    
    @FXML private TableView<Curso> tableView;
    @FXML private TableColumn<Curso, String> titlecolumn;
    @FXML private TableColumn<Curso, String> teachercolumn;
    @FXML private TableColumn<Curso, Integer> limitcolumn;
    @FXML private TableColumn<Curso, LocalDate> startscolumn;
    @FXML private TableColumn<Curso, LocalDate> endscolumn;
    @FXML private TableColumn<Curso, LocalTime> timecolumn;
    @FXML private TableColumn<Curso, ArrayList<Dias>> dayscolumn;
    @FXML private TableColumn<Curso, String> roomcolumn;
    
    @FXML private Button DeleteCourse;
    @FXML private Button Enrollment;
    
    //@FXML private TextField editName;
    //@FXML private TextField editDNI;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        DeleteCourse.disableProperty().bind(Bindings.equal(-1,tableView.getSelectionModel().selectedIndexProperty()));
        Enrollment.disableProperty().bind(Bindings.equal(-1,tableView.getSelectionModel().selectedIndexProperty()));
    }
    
    public void initData ( ObservableList<Curso> cursoslist, ObservableList<Matricula> matriculalist){
        cursosObservable = cursoslist;
        matriculaObservable = matriculalist;
        tableView.setItems(cursosObservable);

        titlecolumn.setCellValueFactory(new PropertyValueFactory<Curso, String>("titulodelcurso"));
        teachercolumn.setCellValueFactory(new PropertyValueFactory<Curso, String>("profesorAsignado"));
        limitcolumn.setCellValueFactory(new PropertyValueFactory<Curso, Integer>("numeroMaximodeAlumnos"));
        startscolumn.setCellValueFactory(new PropertyValueFactory<Curso, LocalDate>("fechainicio"));
        endscolumn.setCellValueFactory(new PropertyValueFactory<Curso, LocalDate>("fechafin"));
        timecolumn.setCellValueFactory(new PropertyValueFactory<Curso, LocalTime>("hora"));
        dayscolumn.setCellValueFactory(new PropertyValueFactory<Curso, ArrayList<Dias>>("diasimparte"));
        roomcolumn.setCellValueFactory(new PropertyValueFactory<Curso, String>("aula"));
    }
    
    
    @FXML
    private void handleDeleteCourse (ActionEvent event) throws IOException {
        
        Alert alertcon = new Alert(Alert.AlertType.CONFIRMATION);
        alertcon.setTitle("Confirmation dialog");
        alertcon.setHeaderText("Student will be removed from the course");
        alertcon.setContentText("Do you want to continue?");

        Optional<ButtonType> result = alertcon.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
        
        int index = tableView.getSelectionModel().selectedIndexProperty().getValue();
        boolean deleteOrNot = true;
        Curso curso = tableView.getSelectionModel().getSelectedItem();
        
        for(Matricula m : matriculaObservable) {
            if(m.getCurso().getTitulodelcurso().equals(curso.getTitulodelcurso())) {    
                //display error
                deleteOrNot = false;
            }
        }
        if (!deleteOrNot) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error dialog");
            alert.setHeaderText("Delete failed");
            alert.setContentText("Students are enrolled to this course");
            alert.showAndWait();
        }
        if (index>=0 && deleteOrNot) cursosObservable.remove(index);
        
        }
    }
    
    @FXML
    private void handleEnrollment (ActionEvent event) throws IOException {
        
            Curso curso = tableView.getSelectionModel().getSelectedItem();
            if (acceso.getAlumnosDeCurso(curso) != null)
                AlumnosEnCursoObservable = FXCollections.observableList(acceso.getAlumnosDeCurso(curso));
            else {
                AlumnosEnCursoObservable = FXCollections.observableList(new ArrayList<Alumno>());
            }
            //Load the IU objects
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource("FXMLEnrollment.fxml"));
            Pane root = (Pane) myLoader.load();
            //Get the controller of the UI
            FXMLEnrollmentController detailsController = myLoader.<FXMLEnrollmentController>getController();          
            //We pass the data to the cotroller. Passing the observableList we 
            //give controll to the modal for deleting/adding/modify the data shown in tableview
            detailsController.initData(matriculaObservable,AlumnosEnCursoObservable,curso);
          
            Scene scene = new Scene (root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Enrollment details");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(true);
            stage.show();
    }
    
    @FXML
    private void handleAdd (ActionEvent event) throws IOException {
        
            //Load the IU objects
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource("FXMLNewCourse.fxml"));
            Pane root = (Pane) myLoader.load();
            //Get the controller of the UI
            FXMLNewCourseController detailsController = myLoader.<FXMLNewCourseController>getController();          
            //We pass the data to the cotroller. Passing the observableList we 
            //give controll to the modal for deleting/adding/modify the data shown in tableview
            detailsController.initData(cursosObservable);
          
            Scene scene = new Scene (root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Add a new course");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(true);
            stage.show();
    }
    
}
