/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivarable1;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.Alumno;
import modelo.Curso;
import modelo.Matricula;

/**
 * FXML Controller class
 *
 * @author Ania
 */
public class FXMLEnrollmentController implements Initializable {

    ObservableList<Matricula> matriculaObservable;
    ObservableList<Alumno> AlumnosEnCursoObservable;
    Curso actualcourse;
    
    @FXML private TableView<Alumno> tableView;
    @FXML private TableColumn<Alumno, String> idcolumn;
    @FXML private TableColumn<Alumno, String> namecolumn;
    @FXML private Button RemoveButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        RemoveButton.disableProperty().bind(Bindings.equal(-1,tableView.getSelectionModel().selectedIndexProperty()));
    }    
    
    public void initData (ObservableList<Matricula> matriculalist, ObservableList<Alumno> alumnoslist, Curso curso){
        matriculaObservable = matriculalist;
        AlumnosEnCursoObservable = alumnoslist;
        actualcourse = curso;
        
        tableView.setItems(AlumnosEnCursoObservable);
        idcolumn.setCellValueFactory(new PropertyValueFactory<Alumno, String>("dni"));
        namecolumn.setCellValueFactory(new PropertyValueFactory<Alumno, String>("nombre"));
    }
    
    @FXML
    private void handleAddStudentToCourse (ActionEvent event) throws IOException {
        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("FXMLAddStudentToCourse.fxml"));
        Pane root = (Pane) myLoader.load();
        //Get the controller of the UI
        FXMLAddStudentToCourseController astcController = myLoader.<FXMLAddStudentToCourseController>getController();          
        //We pass the data to the cotroller. Passing the observableList we 
        //give controll to the modal for deleting/adding/modify the data shown in tableview
        
        astcController.initData(matriculaObservable,AlumnosEnCursoObservable,actualcourse);

        Scene scene = new Scene (root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Add student to course");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(true);
        stage.show();
    }
    
    @FXML
    private void handleRemoveStudent (ActionEvent event) throws IOException {
        
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation dialog");
        alert.setHeaderText("Student will be removed from the course");
        alert.setContentText("Do you want to continue?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
            
            int index = tableView.getSelectionModel().selectedIndexProperty().getValue();
            Alumno alumno = tableView.getSelectionModel().getSelectedItem();
        
            Matricula toDelete = null;
            for(Matricula m : matriculaObservable) {
                if(m.getCurso().getTitulodelcurso().equals(actualcourse.getTitulodelcurso()) && 
                   m.getAlumno().getDni().equals(alumno.getDni()) ) {    
                        //matriculaObservable.remove(m);
                        toDelete = m;
                }
            } 
            if (index>=0 && toDelete != null) {
                AlumnosEnCursoObservable.remove(index);
                matriculaObservable.remove(toDelete);
                toDelete = null;
            }
        }
    }
    
}
