/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivarable1;

import accesoaBD.AccesoaBD;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.Alumno;
import modelo.Curso;
import modelo.Dias;
import modelo.Matricula;

/**
 *
 * @author Jakub Mojto
 */
public class FXMLAddStudentToCourseController implements Initializable {

    AccesoaBD acceso = new AccesoaBD();
    ObservableList<Alumno> AlumnosEnCursoObservable;
    ObservableList<Alumno> alumnosObservable;
    ObservableList<Matricula> matriculaObservable;
    Curso actualcourse;
    
    @FXML private TableView<Alumno> tableView;
    @FXML private TableColumn<Alumno, Integer> idcolumn;
    @FXML private TableColumn<Alumno, String> namecolumn;
    @FXML private Button AddButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        AddButton.disableProperty().bind(Bindings.equal(-1,tableView.getSelectionModel().selectedIndexProperty()));
    }
    
    void initData(ObservableList<Matricula> matriculas, ObservableList<Alumno> AlumnosEnCurso, Curso actual) {
        AlumnosEnCursoObservable = AlumnosEnCurso;
        alumnosObservable = FXCollections.observableList(acceso.getAlumnos());
        tableView.setItems(alumnosObservable);
        matriculaObservable = matriculas;
        actualcourse = actual;
        
        idcolumn.setCellValueFactory(new PropertyValueFactory<Alumno, Integer>("dni"));
        namecolumn.setCellValueFactory(new PropertyValueFactory<Alumno, String>("nombre"));
    }
    
    @FXML
    private void handleFinallyAddStudentToCourse(ActionEvent event) throws IOException {
        int index = tableView.getSelectionModel().selectedIndexProperty().getValue();
        boolean matriculated = false;
        Alumno alumno = tableView.getSelectionModel().getSelectedItem();
       
        for(Alumno a : AlumnosEnCursoObservable) {
            if(a.getDni().equals(alumno.getDni())) {
                //display error
                matriculated = true;
            }
        }
        
        boolean canMatriculate = true;
        
        if (matriculated) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error dialog");
            alert.setHeaderText("Can't add student");
            alert.setContentText("Student is already matriculated to this course");
            alert.showAndWait();
        } else if (actualcourse.getNumeroMaximodeAlumnos() <= AlumnosEnCursoObservable.size()){
            canMatriculate = false;
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error dialog");
            alert.setHeaderText("Can't add student");
            alert.setContentText("Course is full");
            alert.showAndWait();
        } else {
            for(Matricula m : matriculaObservable) {
              if(m.getAlumno().getDni().equals(alumno.getDni())) {   
                  System.out.println(m.getCurso().getDiasimparte());
                  System.out.println(actualcourse.getDiasimparte());
                if(m.getCurso().getFechafin().compareTo(actualcourse.getFechainicio()) >= 0 || m.getCurso().getFechainicio().compareTo(actualcourse.getFechafin()) <= 0) {
                    for(Dias d : actualcourse.getDiasimparte()) {
                        if (m.getCurso().getDiasimparte().contains(d)) //canMatriculate = false;
                        {
                            System.out.println(m.getCurso().getDiasimparte());
                            System.out.println(actualcourse.getDiasimparte());
                            if(m.getCurso().getHora().equals(actualcourse.getHora())) {
                                canMatriculate = false;
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error dialog");
                            alert.setHeaderText("Can't add student");
                            alert.setContentText("Student has a different course at that time");
                            alert.showAndWait();
                            }
                        }
                    }
                }// else { canMatriculate = false; }
              }
            } 
        }
        if (index>=0 && !matriculated && canMatriculate) {
            AlumnosEnCursoObservable.add(alumno);
            matriculaObservable.add(new Matricula(LocalDate.now(), actualcourse, alumno));

        Alert alertinfo = new Alert(Alert.AlertType.INFORMATION);
        alertinfo.setTitle("Information dialog");
        alertinfo.setHeaderText("Student has been added to the course");
        alertinfo.showAndWait();
        }
    }
    
    
    
}
