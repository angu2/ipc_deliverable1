/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivarable1;

import accesoaBD.AccesoaBD;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.Alumno;
import modelo.Curso;
import modelo.Matricula;

/**
 *
 * @author Ania
 */
public class FXMLDocumentController implements Initializable {
    
    
    ObservableList<Alumno> alumnosObservable;
    ObservableList<Curso> cursosObservable;
    ObservableList<Matricula> matriculaObservable;

    @FXML
    private Label label;
    
    @FXML
    private void handleCoursesAction(ActionEvent event)throws Exception {
        //Load the IU objects
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource("FXMLCourses.fxml"));
            Pane root = (Pane) myLoader.load();
            
            //Get the controller of the UI
            FXMLCoursesController detailsController = myLoader.<FXMLCoursesController>getController();          
            //We pass the data to the cotroller. Passing the observableList we 
            //give controll to the modal for deleting/adding/modify the data shown in tableview
            detailsController.initData(cursosObservable,matriculaObservable);
          
            Scene scene = new Scene (root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Courses details");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(true);
            stage.show();
    }
    
    @FXML
    private void handleStudentsAction(ActionEvent event)throws Exception {
        //Load the IU objects
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource("FXMLStudents.fxml"));
            Pane root = (Pane) myLoader.load();
            
            //Get the controller of the UI
            FXMLStudentsController detailsController = myLoader.<FXMLStudentsController>getController();          
            //We pass the data to the cotroller. Passing the observableList we 
            //give controll to the modal for deleting/adding/modify the data shown in tableview
            detailsController.initData(alumnosObservable,matriculaObservable);
          
            Scene scene = new Scene (root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Person details");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(true);
            stage.show();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        AccesoaBD acceso = new AccesoaBD();
        alumnosObservable = FXCollections.observableList(acceso.getAlumnos());
        matriculaObservable = FXCollections.observableList(acceso.getMatriculas());
        cursosObservable = FXCollections.observableList(acceso.getCursos());
    }    
    
    @FXML
    private void handleHelp(ActionEvent event)throws Exception {
        
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Application description");
            alert.setHeaderText("Help");
            alert.setContentText("To desplay list of all students or courses choose the corresponding button.\n"
                    + "In order to manage enrollment go to courses. First you must pick the right course and choose the option Enrollment to obtain students already matriculated and options to update students an the course.");
            alert.showAndWait();
        
        
    }
    
}
