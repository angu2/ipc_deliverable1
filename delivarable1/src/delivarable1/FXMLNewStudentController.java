/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivarable1;

import javafx.scene.image.Image ;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.cell.PropertyValueFactory;
import modelo.Alumno;
import modelo.Matricula;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;


/**
 * FXML Controller class
 *
 * @author Ania
 */
public class FXMLNewStudentController implements Initializable {

    ObservableList<Alumno> alumnosObservable;
    
    @FXML private Button buttonAdd;
    @FXML private TextField editDNI;
    @FXML private TextField editName;
    @FXML private TextField editAge;
    @FXML private TextField editAddress;
    @FXML private DatePicker editDate;
    @FXML private TextField editPhoto;
    @FXML private RadioButton rb1;
    @FXML private RadioButton rb2;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        buttonAdd.disableProperty().bind(Bindings.or(editName.textProperty().isEmpty(),editDNI.textProperty().isEmpty()));
    }    
    
    public void initData ( ObservableList<Alumno> alumnoslist){
        alumnosObservable = alumnoslist;        
    }
    
    @FXML
    private void onActionButtonAdd(ActionEvent event) {         
        
        int intAge = Integer.parseInt(editAge.textProperty().getValue());
        LocalDate date = editDate.getValue();
        if (rb1.isSelected()) {
            alumnosObservable.add(new Alumno(editDNI.textProperty().getValue(), editName.textProperty().getValue(), intAge, editAddress.textProperty().getValue(), date, new Image("/images/man.jpg")));
        } else {
            alumnosObservable.add(new Alumno(editDNI.textProperty().getValue(), editName.textProperty().getValue(), intAge, editAddress.textProperty().getValue(), date, new Image("/images/woman.jpg")));
        }
                       
        ((Node) event.getSource()).getScene().getWindow().hide();
    }
    
    @FXML
    private void onActionButtonCancel(ActionEvent event) {
        //Close the window
        ((Node) event.getSource()).getScene().getWindow().hide();
    }
    
}
