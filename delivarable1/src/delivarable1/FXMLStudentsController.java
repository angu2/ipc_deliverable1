/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivarable1;

import javafx.scene.image.Image;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.Alumno;
import modelo.Curso;
import modelo.Matricula;

/**
 * FXML Controller class
 *
 * @author Ania
 */
public class FXMLStudentsController implements Initializable {

    ObservableList<Alumno> alumnosObservable;
    ObservableList<Matricula> matriculaObservable;
    
    
    @FXML private TableView<Alumno> tableView;
    @FXML private TableColumn<Alumno, Integer> idcolumn;
    @FXML private TableColumn<Alumno, String> namecolumn;
    @FXML private TableColumn<Alumno, Integer> agecolumn;
    @FXML private TableColumn<Alumno, String> addresscolumn;
    @FXML private TableColumn<Alumno, LocalDate> ingressdatecolumn;
    
    @FXML private Button Delete;
    @FXML private Button Photo;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Delete.disableProperty().bind(Bindings.equal(-1,tableView.getSelectionModel().selectedIndexProperty()));
        Photo.disableProperty().bind(Bindings.equal(-1,tableView.getSelectionModel().selectedIndexProperty())); 
    }    
    
    public void initData ( ObservableList<Alumno> alumnoslist, ObservableList<Matricula> matriculalist){
        alumnosObservable = alumnoslist;
        matriculaObservable = matriculalist;
        tableView.setItems(alumnosObservable);
        
        idcolumn.setCellValueFactory(new PropertyValueFactory<Alumno, Integer>("dni"));
        namecolumn.setCellValueFactory(new PropertyValueFactory<Alumno, String>("nombre"));
        agecolumn.setCellValueFactory(new PropertyValueFactory<Alumno, Integer>("edad"));
        addresscolumn.setCellValueFactory(new PropertyValueFactory<Alumno, String>("direccion"));
        ingressdatecolumn.setCellValueFactory(new PropertyValueFactory<Alumno, LocalDate>("fechadealta"));
    }
    
    @FXML
    private void handlePhoto(ActionEvent event) throws IOException {
        Image i = tableView.getSelectionModel().getSelectedItem().getFoto();
        
        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("FXMLPhoto.fxml"));
        Pane root = (Pane) myLoader.load();
        
        FXMLPhotoController photoController = myLoader.<FXMLPhotoController>getController();
        System.out.println(i);
        photoController.initData(i);
        
        Scene scene = new Scene (root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Photo");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(true);
        stage.show();
    }
    
    @FXML
    private void handleDelete(ActionEvent event) throws IOException {
        
        Alert alertcon = new Alert(Alert.AlertType.CONFIRMATION);
        alertcon.setTitle("Confirmation dialog");
        alertcon.setHeaderText("Student will be removed from the course");
        alertcon.setContentText("Do you want to continue?");

        Optional<ButtonType> result = alertcon.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK){
        
        int index = tableView.getSelectionModel().selectedIndexProperty().getValue();
        boolean deleteOrNot = true;
        Alumno alumno = tableView.getSelectionModel().getSelectedItem();
        
        for(Matricula m : matriculaObservable) {
            if(m.getAlumno().getDni().equals(alumno.getDni())) {
                //display error
                deleteOrNot = false;
            }
        }
        if (!deleteOrNot) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error dialog");
            alert.setHeaderText("Delete failed");
            alert.setContentText("Student is matriculated to some course");
            alert.showAndWait();
        }
        if (index>=0 && deleteOrNot) alumnosObservable.remove(index);
        
        }
    }
    
    @FXML
    private void handleAdd (ActionEvent event) throws IOException {
        
            //Load the IU objects
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource("FXMLNewStudent.fxml"));
            Pane root = (Pane) myLoader.load();
            //Get the controller of the UI
            FXMLNewStudentController detailsController = myLoader.<FXMLNewStudentController>getController();          
            //We pass the data to the cotroller. Passing the observableList we 
            //give controll to the modal for deleting/adding/modify the data shown in tableview
            detailsController.initData(alumnosObservable);
          
            Scene scene = new Scene (root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Add a new student");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(true);
            stage.show();
    }
}
