/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package delivarable1;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import modelo.Curso;
import modelo.Dias;

/**
 * FXML Controller class
 *
 * @author Ania
 */
public class FXMLNewCourseController implements Initializable {

    ObservableList<Curso> cursosObservable;
    
    @FXML private Button buttonAdd;
    @FXML private TextField editTitle;
    @FXML private TextField editTeacher;
    @FXML private TextField editLimit;
    @FXML private DatePicker editStartdate;
    @FXML private DatePicker editEnddate;
    @FXML private TextField editTime;
    @FXML private TextField editRoom;
    
    @FXML private RadioButton Mon;
    @FXML private RadioButton Tue;
    @FXML private RadioButton Wed;
    @FXML private RadioButton Thu;
    @FXML private RadioButton Fri;
    @FXML private RadioButton Sat;
    @FXML private RadioButton Sun;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        buttonAdd.disableProperty().bind(Bindings.or(editTitle.textProperty().isEmpty(),editTeacher.textProperty().isEmpty()));
    }    
    
    public void initData ( ObservableList<Curso> cursoslist){
        cursosObservable = cursoslist;
       
    }
    
    @FXML
    private void onActionButtonAdd(ActionEvent event) {         
        
        int intLimit = Integer.parseInt(editLimit.textProperty().getValue());
        LocalDate startdate = editStartdate.getValue();
        LocalDate enddate = editEnddate.getValue();
        LocalTime time = LocalTime.parse(editTime.textProperty().getValue() );
        
        ArrayList<Dias> days = new ArrayList<Dias>();
        if (Mon.isSelected()) days.add(Dias.Lunes);
        if (Tue.isSelected()) days.add(Dias.Martes);
        if (Wed.isSelected()) days.add(Dias.Miercoles);
        if (Thu.isSelected()) days.add(Dias.Jueves);
        if (Fri.isSelected()) days.add(Dias.Viernes);
        if (Sat.isSelected()) days.add(Dias.Sabado);
        if (Sun.isSelected()) days.add(Dias.Domingo);
        
        cursosObservable.add(new Curso(editTitle.textProperty().getValue(), editTeacher.textProperty().getValue(), intLimit, startdate, enddate,time, days, editRoom.textProperty().getValue()));
                       
        ((Node) event.getSource()).getScene().getWindow().hide();
    }
    
    @FXML
    private void onActionButtonCancel(ActionEvent event) {
        //Close the window
        ((Node) event.getSource()).getScene().getWindow().hide();
    }
    
    
}
